#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author：mgboy time:2020/7/26
import sys

from PyQt5.QtGui import QTextCursor
from PyQt5.QtWidgets import QApplication, QMainWindow,QWidget,QDialog
from PyQt5 import QtCore

from textedit_test import Ui_Form

class MyWindow(QWidget, Ui_Form):
    def __init__(self, parent=None):
        super(MyWindow, self).__init__(parent)
        self.setupUi(self)

        # self.textEdit.setPlainText('第一次输入内容')
        # self.textEdit.setPlainText('第二次输入内容')

        self.pushButton.clicked.connect(self.show_text)

    def show_text(self):
        a = '*'
        for i in range(1,50):
            text = '第' + str(i) + '次输入内容' + a * i + '\n'
            self.textEdit.append(text)


if __name__ == "__main__":
    #适配2k等高分辨率屏幕
    QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling)
    app = QApplication(sys.argv)
    myWin = MyWindow()
    myWin.show()
    sys.exit(app.exec_())
